#!/bin/bash

if [[ -z "${SKIP_EDITS}" ]]; then
  trap 'printf "\n\n\e[31mSome project cannot be processed.\e[39m\n   Use \e[33mmake status\e[39m to check out the status of the projects.\n   \e[34mexport SKIP_EDITS=1\e[39m to ignore the errors and go on with the script\n\n"; exit 1' ERR

fi
if [[ "${RESET_REPOS}" ]]; then
  printf "\n\nEnv variable RESET_REPOS is defined. \e[31mThis will delete any your change in tools.\e[39m\n"
  read -r -p "Are you sure? [y/N] " response
  case "$response" in
    [yY][eE][sS]|[yY]) 
        echo "OK"
        ;;
    *)
        printf "Run the command \e[34munset RESET_REPOS\e[39m to avoid the reset of the repositories.\n\n"
        exit 0
        ;;
  esac
fi

# set to 0 to use the db models how defined in the repository,
# set to 1 to use the common definition of the db models. This option should increase compatibility
COMMON_MODELS=0

if [ "$#" -lt 1 ]; then
  services_file="services.txt"
else
  services_file="${1}"
fi

if [ ! -f "$services_file" ]; then
    echo "$services_file does not exist"
    exit 1
fi

projects=""
while IFS= read -r line
do  
  if [[ ! $line =~ "#" ]]; then
    if [ ! -z "$line" ]; then # check not empty
      projects="${projects} ${line}"
    fi
  fi  
done < "$services_file"




input="${services_file}"
projects=""
while IFS= read -r line
do  
  if [[ ! $line =~ "#" ]]; then
    if [ ! -z "$line" ]; then # check not empty
      # remove leading whitespace characters
      line="${line#"${line%%[![:space:]]*}"}"
      # remove trailing whitespace characters
      line="${line%"${line##*[![:space:]]}"}" 
      read -ra service_scale <<< "$line" # split the line into parts
      
      if [ -z "${service_scale[1]}" ]; then
        echo "malformed line: $line"
        exit 1
      fi
      if [ -z "${service_scale[2]}" ]; then
        echo "malformed line: $line"
        exit 1
      fi
      if [ ! -z "${service_scale[3]}" ]; then # check not empty. If not, the second value is the scale factor
        projects="${projects} ${service_scale[1]}" #add the line to projects
        
        declare ${service_scale[0]}"_reponame"=${service_scale[1]} # variable used to store the reponame
        declare ${service_scale[0]}"_repobranch"=${service_scale[2]} #variable used to store the branchname

      else # if the line has a single value it is just the project name
        projects="${projects} ${line}" #add the line to projects
      fi
      
    fi
  fi  
done < "$input"






PROJECT=$dse_engine_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$dse_engine_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd dse_engine/app 
        rm -fr gridsearch
        cd modules 
          rm -fr gridsearch 
          git clone git@gitlab.com:aloha.eu/gridsearch.git -b unica
          cd gridsearch 
            wget -nc -i mongodb_driver_3.10.1.txt -P ./lib 
            mkdir -p bin
          cd ..
          rm -fr ga_aloha 
          git clone git@gitlab.com:aloha.eu/ga_aloha.git
          cd ga_aloha
            mkdir -p bin
          cd ..
          if [ "$COMMON_MODELS" -eq 1 ]; then
            rm -fr models
            git clone git@gitlab.com:aloha.eu/db_models.git
            mv db_models models
          fi
        cd ..
      cd ../..
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull
        cd app/modules
          cd gridsearch
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git checkout unica
            git pull
          cd ..
          cd ga_aloha
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ..
          if [ "$COMMON_MODELS" -eq 1 ]; then
            cd models
              if [[ "${FORCE_PULL}" ]]; then
                git stash
              fi
              git pull
            cd ..
          fi
        cd ../..
      cd ..
  fi
fi
fi

PROJECT=$security_evaluation_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$security_evaluation_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd security_evaluation
        git submodule init
        git submodule update
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app
            rm -fr models
            git clone git@gitlab.com:aloha.eu/db_models.git
            mv db_models models
          cd ..
        fi
      cd ..
      #cd security_evaluation/app
      #git clone git@gitlab.com:aloha.eu/onnxparser.git
      #cd -
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull 
        git submodule update
        
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
  fi
fi
fi


PROJECT=$training_engine_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$training_engine_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd $PROJECT/app/api/engine/utils
        git clone git@gitlab.com:aloha.eu/onnxparser -b master
      cd -
      if [ "$COMMON_MODELS" -eq 1 ]; then
        cd $PROJECT/app
          rm -fr models
          git clone git@gitlab.com:aloha.eu/db_models.git
          mv db_models models
        cd ../..
      fi
      
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        git checkout $BRANCH
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git pull 
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
      printf "\nPulling onnxparser...\n"
      cd $PROJECT/app/api/engine/utils/onnxparser
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git pull
      cd -
  fi
fi
fi


PROJECT=$power_performance_evaluation_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$power_performance_evaluation_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd power_performance_evaluation/app
      git clone git@gitlab.com:aloha.eu/power_perf_sources.git -b main
      rm -fr espamai
      mv power_perf_sources espamai
      if [ "$COMMON_MODELS" -eq 1 ]; then
        rm -fr models
        git clone git@gitlab.com:aloha.eu/db_models.git
        mv db_models models
      fi
      cd ../..
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull 
        printf "\nTrying to pull power_perf_sources...\n"
        cd app/espamai
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git checkout espamAI
          git pull
        cd ../..
        
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
  fi
fi
fi


PROJECT=$refinement_for_parsimonious_inference_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$refinement_for_parsimonious_inference_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then    
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd refinement_for_parsimonious_inference/app
        git clone git@gitlab.com:aloha.eu/rpi_engine.git -b master
        cd rpi_engine
          git submodule init
          git submodule update
          printf "RPI engine needs the DataParser, trying to clone it...\n"
          cd tools
            git clone git@gitlab.com:aloha.eu/training_engine.git -b unica
          cd ..
        cd ..
        if [ "$COMMON_MODELS" -eq 1 ]; then
          rm -fr models
          git clone git@gitlab.com:aloha.eu/db_models.git
          mv db_models models
        fi
      cd ../..
      
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull
        cd app/rpi_engine
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git pull
          git submodule update
        cd ../..
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
  fi
fi
fi


PROJECT=$orchestrator_backend_api_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$orchestrator_backend_api_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then    
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      if [ "$COMMON_MODELS" -eq 1 ]; then
        cd $PROJECT/app
          rm -fr models
          git clone git@gitlab.com:aloha.eu/db_models.git
          mv db_models models
        cd ../..
      fi
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull 
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
  fi
fi
fi


PROJECT=$orchestrator_frontend_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$orchestrator_frontend_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then    
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull 
      cd ..
  fi
fi
fi


PROJECT=$sl_dse_engine_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$sl_dse_engine_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      
      if [ "$COMMON_MODELS" -eq 1 ]; then
        cd $PROJECT/app
          rm -fr models
          git clone git@gitlab.com:aloha.eu/db_models.git
          mv db_models models
        cd ../..
      fi
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
  fi
fi
fi

PROJECT=$sesame_evaluation_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$sesame_evaluation_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd sesame_evaluation/app
        git clone git@gitlab.com:aloha.eu/power_perf_sources.git -b espamAI
        rm -fr espamai
        mv power_perf_sources espamai
        git clone git@gitlab.com:aloha.eu/sesame_dse.git -b $BRANCH
        
        if [ "$COMMON_MODELS" -eq 1 ]; then
          rm -fr models
          git clone git@gitlab.com:aloha.eu/db_models.git
          mv db_models models
        fi
      cd ../..
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull 
        printf "\nTrying to pull power_perf_sources...\n"
        cd app/espamai
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git checkout espamAI
          git pull
        cd ../..
        printf "\nTrying to pull sesame_dse...\n"
        cd app/sesame_dse
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git checkout $BRANCH
          git pull
        cd ../..
        if [ "$COMMON_MODELS" -eq 1 ]; then
          cd app/models
            if [[ "${FORCE_PULL}" ]]; then
              git stash
            fi
            git pull
          cd ../..
        fi
      cd ..
  fi
fi
fi

PROJECT=$netron_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$netron_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull
      cd ..
  fi
fi
fi



PROJECT=$code_generation_reponame
if [[ "${PROJECT}" ]]; then
if [[ "${RESET_REPOS}" ]]; then
  rm -rf $PROJECT
fi
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$code_generation_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "$PROJECT" ] ; then
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      cd code_generation/app
        git clone git@gitlab.com:aloha.eu/power_perf_sources.git -b espamAI
        rm -fr espamai
        mv power_perf_sources espamai
        git clone git@gitlab.com:aloha.eu/convnet.git -b master
        cd convnet/tools
          git clone git@gitlab.com:aloha.eu/onnxparser -b master
        cd ../..
      cd ../..
      
  else
      printf "$PROJECT exists, trying to pull ...\n"
      cd "$PROJECT"
        if [[ "${FORCE_PULL}" ]]; then
          git stash
        fi
        git checkout $BRANCH
        git pull 
        printf "\nTrying to pull power_perf_sources...\n"
        cd app/espamai
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git checkout espamAI
          git pull
        cd ../..
        printf "\nTrying to pull convnet...\n"
        cd app/convnet
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git checkout master
          git pull
        cd ../..
        printf "\nTrying to pull onnxparser...\n"
        cd app/convnet/tools/onnxparser
          if [[ "${FORCE_PULL}" ]]; then
            git stash
          fi
          git checkout master
          git pull
        cd ../../..
      cd ..
  fi
fi
fi



PROJECT=$example_data_reponame
if [[ "${PROJECT}" ]]; then
if [[ $projects =~ $PROJECT ]]; then
  BRANCH=$example_data_repobranch
  printf "\n\n\n\e[33m##################################\n# $PROJECT \n##################################\e[39m\n"
  if [ ! -d "shared_data" ] ; then    
      git clone "git@gitlab.com:aloha.eu/$PROJECT.git" -b $BRANCH
      mv example_data shared_data
      cd shared_data
        #source get_yolo.sh
        git clone git@gitlab.com:aloha.eu/plugins.git
      cd ..
  else
      printf "shared_data folder already exists\n"
      printf "\tClone interrupted to prevent data loss. Do it manually.\n"
      
  fi
fi
fi

