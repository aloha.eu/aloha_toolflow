#!/bin/bash

USAGE="Usage: ./aloha.sh <start|build|service_name|help> [services.txt]\n"

if [ "$#" -lt 1 ]; then
  printf "$USAGE";
  exit 1
fi

if [ "$#" -eq 2 ]; then
  services_file="${2}"
else
  printf "Using default services file: services.txt\n"
  services_file="services.txt"
fi

if [ ! -f "$services_file" ]; then
    echo "$services_file does not exist"
    exit 1
fi

declare -A projects2services
projects2services=(["code_generation"]="code_generation worker_code_generation"
                   ["dse_engine"]="dse_engine worker_dse_engine"
                   ["netron"]="netron"
                   ["orchestrator_backend_api"]="orchestrator_be"
                   ["orchestrator_frontend"]=""
                   ["power_performance_evaluation"]="power_perf worker_power_perf"
                   ["refinement_for_parsimonious_inference"]="parsim_inference worker_parsim_inference"
                   ["security_evaluation"]="security worker_security"
                   ["sesame_evaluation"]="sesame worker_sesame"
                   ["example_data"]=""
                   ["sl_dse_engine"]="sl_dse_engine worker_sl_dse_engine"
                   ["training_engine"]="training celery"
                   ["tensorboard"]="tensorboard"
                   ["parsim"]="parsim")

all_services="mongodb redisdb code_generation worker_code_generation \
dse_engine worker_dse_engine netron orchestrator_be power_perf worker_power_perf \
parsim_inference worker_parsim_inference security worker_security sesame worker_sesame \
sl_dse_engine worker_sl_dse_engine training celery tensorboard parsim"

input="${services_file}"
services="mongodb redisdb"
projects=""
ymls="-f docker-compose.yml"
while IFS= read -r line
do  
  if [[ ! $line =~ "#" ]]; then
    if [ ! -z "$line" ]; then # check not empty
      # remove leading whitespace characters
      line="${line#"${line%%[![:space:]]*}"}"
      # remove trailing whitespace characters
      line="${line%"${line##*[![:space:]]}"}" 
      read -ra service_scale <<< "$line" # split the line into parts
      
      if [ -z "${service_scale[1]}" ]; then
        echo "malformed line: $line"
        exit 1
      fi
      if [ -z "${service_scale[2]}" ]; then
        echo "malformed line: $line"
        exit 1
      fi
      if [ ! -z "${service_scale[3]}" ]; then # check not empty. If not, the second value is the scale factor
        projects="${projects} ${service_scale[0]}" #add the line to projects
        services="${services} ${projects2services[${service_scale[0]}]}"
        if [ ! -z "${projects2services[${service_scale[0]}]}" ]; then
          ymls="${ymls} -f ${service_scale[0]}.yml" #add the docker-compose file 
        fi
        read -ra s <<< "${projects2services[${service_scale[0]}]}" # split the services mapped to a single project. The main service is in first position
        
        export ${s[0]}"_scale"=${service_scale[3]} #export a scale env variable used to scale the number of workers
        export ${s[0]}"_path"="./"${service_scale[1]}"/app" #export a path env variable 
       
      else # if the line has a single value it is just the project name
        projects="${projects} ${line}" #add the line to projects
        services="${services} ${projects2services[${line}]}"
        if [ ! -z "${projects2services[${line}]}" ]; then
          ymls="${ymls} -f ${line}.yml" #add the docker-compose file 
        fi
      fi
      
    fi
  fi  
done < "$input"

#echo "$projects"

#echo "$services"

echo "${projects2services[0][0]}"


if [[ $1 =~ ^start$ ]]; then
  
  printf '\33[H\33[2J' #clears screen saving the scrollback buffer

  printf "###################################\n"
  printf "     Starting ALOHA Toolflow!      \n"
  printf "###################################\n\n"

  printf "  \e[1;34m                                                _---~~(~~-_.\n"
  printf "                                                _{        )   )\n"
  printf "   █████  ██       ██████  ██   ██  █████     ,   ) -~~- ( ,-' )_\n"
  printf "  ██   ██ ██      ██    ██ ██   ██ ██   ██   (  '-,_..'., )-- '_,)\n"
  printf "  ███████ ██      ██    ██ ███████ ███████  ( ' _)  (  -~( -_ ',  }\n"
  printf "  ██   ██ ██      ██    ██ ██   ██ ██   ██  (_-  _  ~_-~~~~',  ,' )\n"
  printf "  ██   ██ ███████  ██████  ██   ██ ██   ██    '~ -^(    __;-,((()))\n"
  printf "                                                    ~~~~ {_ -_(())\n"
  printf "                                                           '\  }\n"
  printf "                                                             { }  \n\e[m"
  printf "               Enjoy it!\n\n"
  printf "using services file: $services_file\n\n"

  sleep 3
  
  docker-compose $ymls up $services
  
elif [[ $1 =~ ^build$ ]]; then
  
  printf '\33[H\33[2J' #clears screen saving the scrollback buffer

  printf "##########################################\n"
  printf "      Building up the ALOHA Toolflow      \n"
  printf "##########################################\n\n"

  printf "Please wait...\n\n"
  printf "using services file: $services_file\n\n"

  sleep 2
  docker-compose $ymls build $services
  
elif [[ $all_services =~ (^| )$1($| ) ]]; then
  printf "##########################################\n"
  printf "      Starting the service $1      \n"
  printf "##########################################\n\n"

  printf "Please wait...\n\n"
  printf "using services file: $services_file\n\n"

  sleep 2
  docker-compose -f docker-compose.yml -f $1.yml up $1
  
elif [[ $1 =~ ^help$ ]]; then
  printf "##########################################\n"
  printf "            ALOHA TOOLFLOW HELP      \n"
  printf "##########################################\n\n"
  
  printf " $USAGE\n\n";
  printf " \e[1;31mThis script requires at least BASH version 4.0, your version is ${BASH_VERSION}\e[m\n\n"
  
  printf " \e[1;34m./aloha.sh build\e[m                    --> builds the ALOHA Toolflow using the default service file\n"
  printf " \e[1;34m./aloha.sh start\e[m                    --> starts the ALOHA Toolflow using the default service file\n"
  printf " \e[1;34m./aloha.sh build services_file.txt\e[m  --> builds the ALOHA Toolflow using the service file named services_file.txt\n"
  printf " \e[1;34m./aloha.sh start services_file.txt\e[m  --> starts the ALOHA Toolflow using the service file named services_file.txt\n\n"
  printf " \e[1;34m./aloha.sh service_name\e[m             --> starts a specific service of the ALOHA Toolflow\n"
  printf "     example: \e[1;34m./aloha.sh tensorboard\e[m --> starts the tensorboard server\n"
  printf "     valid services are: $all_services\n\n"
  printf " \e[1;33mPlease refere to: https://gitlab.com/aloha.eu/aloha_toolflow for further documentation\e[m\n\n"
  
  
  
else
  printf "$USAGE";
  exit 1
fi
