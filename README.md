# ALOHA Toolflow
This project implements the containerised architecture with all the tools, the DSE engines, the mongoDB, the RedisDB.
The architecture is composed by two main parts:
1. a React based frontend that will run on your browser. We'll refer to it as [orchestrator-frontend](#orchestrator)
2. a dockerized backend that will be in charge of processing the projects offloaded by the frontend

After the following steps you should be able to interact with the ALOHA toolflow through your browser.

![ALOHA_architecture](docs/ALOHA_architecture.png)

## Prerequisites
The toolflow has been tested only on an **Ubuntu 16.04 LTS** installation but theoretically it should work on Windows too.

1. [Docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/)
2. [NodeJS 10.13 or higher and npm](https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions)
3. Bash4. Ubuntu is shipped with Bash4 already installed but [MAC OS uses an outdated version of Bash](https://itnext.io/upgrading-bash-on-macos-7138bd1066ba), please upgrade it!
4. A GitLab account with a registered public key. Follow the instructions [here](https://gitlab.com/profile/keys)

**NOTE:** we strongly recommend installing docker following the official documentation. The docker version in the Ubuntu repository could lead to malfunctions.  
To have a friendly experience with docker on Ubuntu OS please [enable the non-root users to run docker](https://docs.docker.com/install/linux/linux-postinstall/) and [enable the auto-completion](https://docs.docker.com/compose/completion/)


To install NodeJS and npm in Ubuntu these two commands are enough:
```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Tested on Docker version 18.09.3 and docker-compose version 1.23.2

**NOTE GPU:** you need to install ```docker-nvidia2``` in order to use the NVIDIA GPU acceleration during the training.  
Follow the instructions: https://github.com/nvidia/nvidia-docker/wiki/Installation-(version-2.0)

## Installation of the dockerized architecture (backend)

First thing **clone this project** 
```
git clone git@gitlab.com:aloha.eu/aloha_toolflow.git
cd aloha_toolflow
```
then follow the instrunctions below.

The ALOHA architecture is composed by multiple tools and each of them has its own project in the ALOHA repository.

The `install_aloha.sh` script will clone all the needed projects. The working directory have to be the root directory of the `aloha_toolflow` project.  
It uses a text file which *lists all the services you want to install and use*. This file enables you to install a subset of the toolflow. You can find some example in the root directory.

```
./install_aloha.sh [services.txt]
```
or  
```
bash install_aloha.sh [services.txt]
```
the install script also creates a folder that will contain the data shared among the tools.  
To run the bash scripts with the `./` notation you have to change file permissions.

NOTE: The firewall rules of your network could limit the traffic on the ssh port causing malfunctioning during the installation.

To know the local repository status of all the repositories run ```make status```  
It explores the whole installation tree and prints the status of every found project.  
NOTE: It generates an intense traffic on port 22 and some firewalls may block the execution.

To update the architecture to the last commits use ```make pull```. You can get errors in case of conflicts with your edits. In this case you have to check them individually.

To reset the dockerized architecture use ```make reset```. Be careful, this command will erase all your changes!


### Building the architecture
The first build may take a while, even 1 hour and more. Ensure you have enough disk space (10 GB should be ok for the first install).

If needed edit the parameters of your installation in the hidden file ```aloha_toolflow/.env``` then run the command:  
```
./aloha.sh build [services.txt]
```
or  
```
bash aloha.sh build [services.txt]
```
Make sure you pass the same `services` file used for the installation or a services file listing a subset of the installed tools. The default services file is *services.txt*.
To enable the GPU acceleration [see below](#gpu-acceleration)

NOTE: Minimum RAM dedicated to Docker must be set up at 5GB to run properly. 

## Orchestrators' addresses
The orchestrator frontend communicates with the orchestrator backend using the addresses set in the file ```aloha_toolflow/orchestrator-frontend/src/configuration/config.js```.
By default it is supposed the frontend and the backend run in the same machine so the value returned by `window.location.hostname` should be ok.

```
export const ALOHA_FRONTEND_CONFIGURATION = {
    orchestratorAPIUrl: 'http://'+window.location.hostname+':8080/',
    tensorboardUrl: 'http://'+window.location.hostname+':8888/',
    netronUrl: 'http://'+window.location.hostname+':8000/',
    version: '0.9.8'
};
```  
If needed you can edit it as you prefer. The example below refers to the backend listening at the port 5000 in the localhost
```
export const ALOHA_FRONTEND_CONFIGURATION = {
    orchestratorAPIUrl: 'http://localhost:5000/',
    tensorboardUrl: 'http://localhost:6000/',
    netronUrl: 'http://localhost:8000/',
    version: '0.8'
};
```
NOTE: The ports must be the same you chose in ```aloha_toolflow/.env```

Edit or create the file ```aloha_toolflow/orchestrator-frontend/.env``` with the public port for the Orchestrator Frontend.
For example if you want the Frontend webserver listening at the port 80 add to the .env file the line:
PORT=80

The backend and the frontend ports must be different, for example 8080 and 80.

The Orchestrator backend starts when you bring up the dockerized architecture 

### Webserver (orchestrator frontend) installation

After the installation of NodeJS and npm, install the frontend's dependencies:
```
cd orchestrator-frontend/
npm install
cd ..
```

## Running ALOHA
For a better experience run the two following commands in two separate terminals.

To bring up the entire architecture
```
./aloha.sh start [services.txt]
```
or  
```
bash aloha.sh start [services.txt]
```
Make sure you pass the same *services file* used for the build step or a services file listing a subset of the available services. The default services file is *services.txt*.

Running this command a lot of informations will be printed. Each tool (and thus each container) is identified by a name. The corresponding standard output starts with its name.
The startup process is completed if no errors are returned and only sparse status information are printed.
![docker_up](docs/docker_up.png)

To launch the Orchestrator Frontend run this command in a new terminal

```
sudo ./webserver.sh
```
or  
```
sudo source webserver.sh
```
```sudo``` is required if you want to use the port 80 or any other port under the 1024. If you select the port i.e. 8088 you can lunch the frontend without root privileges.
The server is running if no error is returned. The server runs in background so you can close or reuse the terminal for example for [monitoring](#monitoring-the-docker-containers)
![webserver](docs/webserver.png)

### First run

On the first run of ALOHA is necessary to visit http://localhost/install to set up the environment.  
In a remote installation localhost should be replaced with the server address.

![GUI_example](docs/install_database.png)

After that you can proceed with the registration of a new user.

After login you'll get a GUI similar to the one in the image below

![GUI_example](docs/aloha_GUI.png)

### Services files
The ALOHA tool flow is composed by multiple tools. The number of tools you'll use depends on the type of explorantion you want to perform.  
The full tool flow may consume a lot of resource on your machine, the services file enables you to select which tools you want to install and run simply listing the names of the projects.
In order to speedup the execution of multiple projects you can also define the number of workers for each service - if they have one - by adding the value after the project name.

The hash char is used to insert comments.  
The following is an example of a services file
```
# List here the services you want to use
# the hash char invalidate the entire line use it to insert comments.
# the gitlab projects' names have to be used

code_generation 2
dse_engine 4
#netron
orchestrator-backend-api
orchestrator-frontend
power_performance_evaluation 2
refinement_for_parsimonious_inference 4
security_evaluation 2
#sesame_evaluation
#example_data
#sl_dse_engine
training_engine 1
#tensorboard
#parsim
```

In the example above ALOHA will run with only the services `code_generation`, `dse_engine`, `orchestrator-backend-api`, `orchestrator-frontend`, `power_performance_evaluation`, `power_performance_evaluation`, `refinement_for_parsimonious_inference`, `security_evaluation`, `training_engine`
with the specified number of workers.

## Populating the shared data folder
The repository "example data" contains sample nets and datasets.  
For debug purposes you can use the gridsearch project. The [unica branch](https://gitlab.com/aloha.eu/gridsearch/tree/unica) has a standalone version that enables you to create onnx models.


## Monitoring the docker containers and debugging

Once started you can connect to the MongoDB and the RedisDB through the ports you chose.

Use a tool like robo3T to browse the MongoDB (https://robomongo.org/download)

Use ```redis-cli``` to browse the RedisDB

To list all the containers:
```docker container ls```
It will show IDs, Names, PORTs, etc...

To force stop of all the containers:
```docker stop $(docker ps -a -q)```

To print the stdout of a container:
```docker logs container_name -f```

To login into a container using bash:
```docker exec -it name_of_the_container /bin/bash```

To show the resource usage of all the containers:
```docker stats -a```

To free up disk space (Be careful, this command may erase your data stored in the containers.):
```docker system prune```

The training engine produces a HDF5 archive to store the dataset used for a training. You can check its content with the [HDF5 viewer](https://www.hdfgroup.org/downloads/hdfview/) tool.

## GPU acceleration

The training tool is enabled to GPU acceleration with the NVIDIA CUDA toolkit on NVIDIA GPU.

**Requirements:**
1. GPU card with minimum CUDA compute capability 3.0
2. Linux x86_64 Driver Version >= 384.81

To enable the acceleration you have to set the environment variable DOCKER\_TRAIN\_RUNTIME to ```nvidia```.  
In a bash shell run the command
```
export DOCKER_TRAIN_RUNTIME=nvidia
```
If the machine that is going to run the celery container has more than one GPU you may select one of them setting its id in the .env file, for example:
```
GPU_ID=1
```
then build the architecture.

**NOTE:** The environment variable will be lost closing the terminal. To make the configuration persistent add the command above to your .bashrc file or uncomment the respective line in the .env file.

**NOTE:** Check if your machine is able to run a GPU accelerated container running the following command:
```
docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
```
Here are the available nvidia/cuda images: https://hub.docker.com/r/nvidia/cuda/tags select a tag compatible with your graphic card.
*The tag is the part after the colon*

the output should be something like this:
```
Fri May 24 10:40:48 2019
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 384.130                Driver Version: 384.130                   |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce MX150       Off  | 00000000:01:00.0 Off |                  N/A |
| N/A   54C    P3    N/A /  N/A |    289MiB /  2001MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
+-----------------------------------------------------------------------------+

```
If you get errors you have to fix something with docker-nvidia or your graphic card is not supported.


## Paths
In the ALOHA dockerized architecture the files are stored in a shared data folder. The files can be onnx models, logs, architecture descriptions, pictures, regimes for the rpi and so on.
The current configuration works only if all the tools run on the same machine because they share the file system. Future improvements will move the shared data in a remote server and the tools will use a network file system.

In the current case the paths must start from a common root directory for all the tools.
In the current case the root directory is the shared folder: when you fill a field in the GUI the path to the shared data folder must be removed from the absolute path of your file.

For instance:  
local architecture description file: /home/user/myaloha_toolflow/shared_data/architectures/myarchs/arch.json  
ALOHA point of view: architectures/myarchs/arch.json  

Your dataset folder: /home/user/myaloha_toolflow/shared_data/datasets/mydataset  
ALOHA point of view: datasets/mydataset  

## Running a project

Once the installation is concluded you should be enabled to create a new project.
The GUI is divided in multiple lanes, the lane currently supported is the first. You can drag the cards over the other lanes but nothing will happen.
To start a new project click the button
![create](docs/create.png)

A popup appears, choose a title and a description.
![createproject](docs/createproject.png)

### Settings

Three tabs enable the user to set additional constraints:
+ Constraints
+ Architecture
+ Algorithm Configuration

#### Constraints
The constraints are the desired requirements the algorithm should have.
Select a value for security, energy consumption, accuracy and security.
At this moment the priority is not supported.

You can choose also which tool to execute.

#### Architecture
This tab enables the user to select a specific target architecture. This selection affects only the power performance tool. You have to specify the path ([how are the paths managed?](#paths)) to a JSON file stored in the shared data folder.
Sample architecture files are available in the example_data project.

#### Algorithm Configuration
This tab is divided in three parts:

**Dataset.** Select the dataset on which the net will be modeled. You can use a known dataset (MNIST, CIFAR) or a custom dataset.
For a custom dataset you have to specify a file or a folder. The custom dataset should respect the indication in the data standardization project.

**Learning algorithm.** Depending on the task you require and the effort you want to spend on select the hyper parameters for the training of the net.
These parameters will affect the behavior of the training engine and the execution time.

**Images.** Set the attributes of the images. Some of these controls may not work yet. 

**Preset configurations.** The user can choose a preset configuration for the three use cases: KWS, Surveillance and Imaging. When you choose a use case the preset parameters will change in the GUI.  
The preset values are set in the file ```projectCreator.js``` in the orchestrator-frontend component (aloha_toolflow/orchestrator-frontend/src/project/projectCreator.js).  
There is a JavaScript function for each use case, you can edit the values defined there.  
If you like, you can still edit the parameters in the backend.

### Submit and start
Clicking submit a new card representing the project is created. To start the evaluation click start. The stop button is not working yet.
![card](docs/card.png)

Once started you can follow the progress in tensorboard by clicking in the Tensorboard button.
The GUI will notify the end of the whole elaboration, detailed information can be retrieved from the terminal running the dockerized architecture.
![getresults](docs/getresults.png)

**NOTE:** At this moment you will not be notified for a failed project. The card could stuck in the running state even it the toolflow crashes.

If a project finish with success you can get the results through a detailed tab
![results](docs/results.png)

### Delete a project
You can delete a project by clicking on the trash bin icon. This action cannot be undone.  
The files in the shared folder are preserved.

### Tensorboard
TensorBoard is a suite of web applications for inspecting and understanding your deep learning runs and graphs.
https://github.com/tensorflow/tensorboard/blob/master/README.md

During the training and the rpi, tensorboard logs are created.
You can check the progress by clicking on the button Tensorboard of the project's card. The server is listening on the port you chose in ```.env``` file.

![tensorboard_example](docs/tb_example.png)

You can use regex expressions to compare different runs

Regex builder: https://regexr.com/

