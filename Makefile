all: clone

clone:
	bash install_aloha.sh services.txt

webserver:
	cd orchestrator-frontend && npm start &
	
	
deprecated_status:
	@printf "\n\n\n##################################\n# docker_arch \n##################################\n"&& git remote update && git status
	@printf "\n\n\n##################################\n# dse_engine \n##################################\n" && cd dse_engine && git remote update && git status && cd ..
	@printf "\n\n\n##################################\n# security_evaluation \n##################################\n" && cd security_evaluation && git remote update && git status && cd ..
	@printf "\n\n\n##################################\n# training_engine \n##################################\n" && cd training_engine && git remote update && git status && cd ..
	@printf "\n\n\n##################################\n# power_performance_evaluation \n##################################\n" && cd power_performance_evaluation && git remote update && git status && cd ..
	@printf "\n\n\n##################################\n# refinement_for_parsimonious_inference \n##################################\n" && cd refinement_for_parsimonious_inference && git remote update && git status && cd ..
	@printf "\n\n\n##################################\n# orchestrator-backend-api \n##################################\n" && cd orchestrator-backend-api && git remote update && git status && cd ..
	@printf "\n\n\n##################################\n# orchestrator-frontend \n##################################\n" && cd orchestrator-frontend && git remote update && git status && cd ..

status:
	@find . ! -readable -prune -o -name ".git" -type d -exec bash -c "printf '\n\n\n#################################################\n' && cd '{}'/.. && pwd && printf '#################################################\n\n' && git remote update && git status" \;
	@printf "The status of all the git repositories in the docker_arch folder has been printed.\n"

branches:
	@find . ! -readable -prune -o -name ".git" -type d -exec bash -c "cd '{}'/.. && pwd && git branch | grep \"*\"" \;
	@printf "The current branch of all the git repositories in the docker_arch folder has been printed.\n"

pull:
	git pull
	bash install_aloha.sh services.txt

reset:
	@printf "This will erase ALL your not pushed to origin edits. Are you sure? [y/N] " && read ans && [ $${ans:-N} == y ]
	rm -rf dse_engine
	rm -rf security_evaluation
	rm -rf training_engine
	rm -rf power_performance_evaluation
	rm -rf refinement_for_parsimonious_inference
	rm -rf orchestrator-backend-api
	rm -rf orchestrator-frontend
	@printf "shared data folder hasn't been erased. Do sudo rm -rf shared_data if needed.\n"
	@printf "run the command make clone to get the projects\n"

.PHONY: clone webserver status pull reset
