# List here the services you want to use
# the hash char invalidate the entire line, use it to insert comments.
# 
# Columns report these information. If empty the default scale factor is 1
# toolname repository_name branch scale_factor
#
#code_generation code_generation master 1
dse_engine dse_engine master 1
#netron netron master 1
orchestrator_backend_api orchestrator-backend-api new_TE 1
orchestrator_frontend orchestrator-frontend unica 1
power_performance_evaluation power_performance_evaluation unica 1
refinement_for_parsimonious_inference refinement_for_parsimonious_inference unica 1
security_evaluation security_evaluation onnxparser 1
#sesame_evaluation sesame_evaluation unica 1
example_data example_data master 1
#sl_dse_engine sl_dse_engine master 1
training_engine training_engine_v2.0 integration 1
#tensorboard tensorboard master 1
#parsim parsim master 1
